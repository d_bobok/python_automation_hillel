"""
Напишіть функцію, що приймає один аргумант. Функція має вивести на екpан тип цього аргумента (для визначення типу використайте type)
"""

import random

first_test = [1, 2.0, 'Python is Greate', True, 7, None, {'country': 'England', 'capital': 'London'}, 'Python is cool', [0, 'for testing purposes'], ('QA', 'BA', 'PM'), '-3']
second_test = [3, 'hillel', 20, -20, {'population': 198537, 'status': True}, -9,99, ['Lenovo', 'Asus'], 8.88, 'qa automation', False, ('test', 'data'), 41.0, 7895]
random_first_test = random.choice(first_test)
random_second_test = random.choice(second_test)
print(f'random_first_test_value ==> {random_first_test}')
print(f'random_second_test_value ==> {random_second_test}', end='\n\n')

def type_of_elem (elem):
    return type(elem)

type_of_test_data = type_of_elem(random_first_test)
print(f'The type of the element "{random_first_test}" is {type_of_test_data}', end='\n\n')

"""
Напишіть функцію, що приймає один аргумент будь-якого типу та повертає цей аргумент, перетворений на float. Якщо перетворити не 
вдається функція має повернути 0 (флоатовий нуль).
"""

def change_to_float (elem):
    try:
        changed_elem = float(elem)
        return changed_elem
    except:
        return float(0)

res_to_float = change_to_float(random_second_test)
print(f'An attempt to cast the element {random_second_test} ({type(random_second_test)}) to the float type gave the following result:\n{res_to_float}', end='\n\n')

"""
Напишіть функцію, що приймає два аргументи. Функція повинна
    якщо аргументи відносяться до числових типів - повернути різницю цих аргументів,
    якщо обидва аргументи це строки - обʼєднати в одну строку та повернути
    якщо перший строка, а другий ні - повернути dict де ключ це перший аргумент, а значення - другий
    у будь-якому іншому випадку повернути кортеж з цих аргументів
"""

def arguments_comparison(first_arg, second_arg):
    if ((type(first_arg)  is int) or (type(first_arg) is float)) and ((type(second_arg) is int) or (type(second_arg) is float)):
        return (first_arg - second_arg)
    elif type(first_arg) is str and type(second_arg) is str:
        return (first_arg, second_arg)
    elif type(first_arg) is str:
        return ({first_arg:second_arg})
    else:
        list_of_elem = []
        list_of_elem.append(first_arg)
        list_of_elem.append(second_arg)
        return (tuple(list_of_elem))

result_comparison = arguments_comparison(random_first_test, random_second_test)
print(f'The first arument is {random_first_test} ({type(random_first_test)}). The second argument is {random_second_test} ({type(random_second_test)}).'
      f' So, result of the programm is:\n{result_comparison}')