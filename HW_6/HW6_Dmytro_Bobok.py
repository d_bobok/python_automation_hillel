'''
Hапишіть программу "Касир в кінотеатрі", яка буде виконувати наступне:

Попросіть користувача ввести свсвій вік.

- якщо користувачу менше 7 - вивести "Тобі ж <>! Де твої батьки?"
- якщо користувачу менше 16 - вивести "Тобі лише <>, а це е фільм для дорослих!"
- якщо користувачу більше 65 - вивести "Вам <>? Покажіть пенсійне посвідчення!"
- якщо вік користувача складається з однакових цифр (11, 22, 44 і тд років, всі можливі варіанти!) - вивести "О, вам <>! Який цікавий вік!"
- у будь-якому іншому випадку - вивести "Незважаючи на те, що вам <>, білетів всеодно нема!"

Замість <> в кожну відповідь підставте значення віку (цифру) та правильну форму слова рік

Для будь-якої відповіді форма слова "рік" має відповідати значенню віку користувача.

Наприклад :

"Тобі ж 5 років! Де твої батьки?"
"Вам 81 рік? Покажіть пенсійне посвідчення!"
"О, вам 33 роки! Який цікавий вік!"

Зробіть все за допомогою функцій! Для кожної функції пропишіть докстрінг або тайпхінтінг.
Не забувайте що кожна функція має виконувати тільки одну завдання і про правила написання коду.

P.S. Для цієї і для всіх наступних домашок пишіть в функціях докстрінги або хінтінг
'''

def get_data_from_user() -> int:
    """
    The function takes input from the user and turns it into type - int. The function runs until the user enters data that can be converted to an int.
    :return: int
    """
    while True:
        try:
            age = int(input('Введіть свій вік, будь ласка: '))
            if age > 0:
                return age
            # else:
            #     print('ERROR! Ваш вік повинен буди бульше ніж нуль!')
        except:
            print('ERROR! Введіть свій вік у форматі цілого числа')


def generate_age_word(age: int) -> str:
    """
    The function determines which word should be added to the user's age: рік, роки, років. The function returns a
    string that consists of the age and the desired word
    :param age: int
    :return: str
    """
    if (age % 10 == 1) and (age != 11) and (age != 111):
        return (f'{age} рік')
    elif (age % 10 > 1) and (age % 10 < 5) and (age != 12) and (age != 13) and (age != 14):
        return (f'{age} роки')
    else:
        return (f'{age} років')


def processing_age_values(user_age: int) -> str:
    """
    The function determines which message to display to the user based on their age.
    The message is also formed using the function "generate_age_word" from where the age and the desired age word are substituted
    :param user_age: int
    :return: str
    """
    age_to_str = str(user_age)
    if len(age_to_str) > 1 and age_to_str.count(age_to_str[0]) == len(age_to_str):
        return (f'О, вам {generate_age_word(user_age)}! Який цікавий вік!')
    elif user_age < 7:
        return (f'Тобі ж {generate_age_word(user_age)}! Де твої батьки?')
    elif user_age < 16:
        return (f'Тобі лише {generate_age_word(user_age)}, а це фільм для дорослих!')
    elif user_age > 65:
        return (f'Вам {generate_age_word(user_age)}? Покажіть пенсійне посвідчення!')
    else:
        return (f'Незважаючи на те, що вам {generate_age_word(user_age)}, білетів всеодно нема!')

result = processing_age_values(get_data_from_user())
print(result)