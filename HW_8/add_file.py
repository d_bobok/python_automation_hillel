def get_data_from_user() -> int:
    """
    The function takes input from the user and turns it into type - int. The function runs until the user enters data that can be converted to an int.
    :return: int
    """
    while True:
        try:
            age = int(input('Введіть свій вік, будь ласка: '))
            if age > 0:
                return age
            else:
                print('ERROR! Ваш вік повинен буди бульше ніж нуль!')
        except:
            print('ERROR! Введіть свій вік у форматі цілого числа')


def generate_age_word(age: int) -> str:
    """
    The function determines which word should be added to the user's age: рік, роки, років. The function returns a
    string that consists of the age and the desired word
    :param age: int
    :return: str
    """
    if (age % 10 == 1) and (age != 11) and (age != 111):
        return f'{age} рік'
    elif (age % 10 > 1) and (age % 10 < 5) and (age != 12) and (age != 13) and (age != 14):
        return f'{age} роки'
    else:
        return f'{age} років'

