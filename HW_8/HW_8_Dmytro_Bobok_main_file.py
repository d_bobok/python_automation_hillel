"""
Візьміть свою HW 6 ("Касир в кінотеатрі"), винесіть всі допоміжні функціі в окремий файл. В основному файлі виконайте імпорт цих функцій.
"""
import add_file
"""
since only 2 functions are found and used in the "add_file.py", I import the entire file as a whole, if this file had
many functions of which I would need only 2, then the import would look like this:
from add_file import get_data_from_user
from add_file import generate_age_word
"""

def processing_age_values(user_age: int) -> str:
    """
    The function determines which message to display to the user based on their age.
    The message is also formed using the function "generate_age_word" from where the age and the desired age word are substituted
    :param user_age: int
    :return: str
    """
    age_to_str = str(user_age)
    if len(age_to_str) > 1 and age_to_str.count(age_to_str[0]) == len(age_to_str):
        return f'О, вам {add_file.generate_age_word(user_age)}! Який цікавий вік!'
    elif user_age < 7:
        return f'Тобі ж {add_file.generate_age_word(user_age)}! Де твої батьки?'
    elif user_age < 16:
        return f'Тобі лише {add_file.generate_age_word(user_age)}, а це фільм для дорослих!'
    elif user_age > 65:
        return f'Вам {add_file.generate_age_word(user_age)}? Покажіть пенсійне посвідчення!'
    else:
        return f'Незважаючи на те, що вам {add_file.generate_age_word(user_age)}, білетів всеодно нема!'

result = processing_age_values(add_file.get_data_from_user())
print(result)

